<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default Queue Driver
	|--------------------------------------------------------------------------
	|
	| The Laravel queue API supports a variety of back-ends via an unified
	| API, giving you convenient access to each back-end using the same
	| syntax for each one. Here you may set the default queue driver.
	|
	| Supported: "sync", "beanstalkd", "sqs", "iron", "redis"
	|
	*/

	'default' => getenv('QUEUE_DRIVER') ?: 'sync',

	/*
	|--------------------------------------------------------------------------
	| Queue Connections
	|--------------------------------------------------------------------------
	|
	| Here you may configure the connection information for each server that
	| is used by your application. A default configuration has been added
	| for each back-end shipped with Laravel. You are free to add more.
	|
	*/

	'connections' => array(

		'sync' => array(
			'driver' => 'sync',
		),

		'beanstalkd' => array(
			'driver' => 'beanstalkd',
			'host'   => getenv('QUEUE_BS_HOST') ?: 'localhost',
			'queue'  => getenv('QUEUE_BS_QUEUE') ?: 'default',
			'ttr'    => getenv('QUEUE_BS_TTR') ?: 60,
		),

		'sqs' => array(
			'driver' => 'sqs',
			'key'    => getenv('QUEUE_SQS_KEY'),
			'secret' => getenv('QUEUE_SQS_SECRET'),
			'queue'  => getenv('QUEUE_SQS_QUEUE'),
			'region' => getenv('QUEUE_SQS_REGION') ?: 'us-east-1',
		),

		'iron' => array(
			'driver'  => 'iron',
			'host'    => getenv('QUEUE_IRON_HOST') ?: 'mq-aws-us-east-1.iron.io',
			'token'   => getenv('QUEUE_IRON_TOKEN'),
			'project' => getenv('QUEUE_IRON_PROJECT'),
			'queue'   => getenv('QUEUE_IRON_QUEUE'),
			'encrypt' => true,
		),

		'redis' => array(
			'driver' => 'redis',
			'queue'  => getenv('QUEUE_REDIS_QUEUE'),
		),

	),

	/*
	|--------------------------------------------------------------------------
	| Failed Queue Jobs
	|--------------------------------------------------------------------------
	|
	| These options configure the behavior of failed queue job logging so you
	| can control which database and table are used to store the jobs that
	| have failed. You may change them to any database / table you wish.
	|
	*/

	'failed' => array(

		'database' => 'mysql', 'table' => 'failed_jobs',

	),

);

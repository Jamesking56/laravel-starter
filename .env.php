<?php

/**
 * Production Environment file
 * Configuration for the 'production' environment. You can check what environment you are detected as by doing 'php artisan env' in a terminal.
 * If the environment is incorrect, please edit /bootstrap/start.php in this project and add your computer name / hostname.
 **/

return [

    // Has the site launched yet?
    'LAUNCHED'              => false, // True if we've launched!

    // Debug Mode
    'DEBUG'					=> false, // Set to true to show exceptions, false to show 'Whoops' page instead.

    // Database
    'DB_TYPE'				=> 'mysql', // mysql, sqlite, pgsql or sqlsrv
    'DB_HOST'				=> 'localhost',
    'DB_USER'				=> '',
    'DB_PASS'				=> '',
    'DB_NAME'				=> '', // If using sqlite, this is the file path. (Starting from app/config)

    // URL
    'SITE_URL'				=> 'http://www.example.com', // This will be used to generate paths so choose wisely.

    // Mail
    'MAIL_DRIVER'			=> 'mandrill', // mail, smtp or sendmail
    'MAIL_SMTP_HOST'		=> '',
    'MAIL_SMTP_PORT'		=> 587,
    'MAIL_FROM_ADDRESS'		=> '',
    'MAIL_FROM_NAME'		=> '',
    'MAIL_ENCRYPTION'		=> 'tls',
    'MAIL_SMTP_USERNAME'	=> '',
    'MAIL_SMTP_PASSWORD'	=> '',
    'MAIL_PRETEND'			=> true, // true to not actually send emails, false to actually send them.

    // Queue
    'QUEUE_DRIVER'			=> 'sync', // sync (no queue), beanstalkd, sqs or iron
    'QUEUE_BS_HOST'			=> '',
    'QUEUE_BS_QUEUE'		=> '',
    'QUEUE_BS_TTR'			=> 60,
    'QUEUE_SQS_KEY'			=> '',
    'QUEUE_SQS_SECRET'		=> '',
    'QUEUE_SQS_QUEUE'		=> '',
    'QUEUE_SQS_REGION'		=> '',
    'QUEUE_IRON_HOST'		=> '',
    'QUEUE_IRON_TOKEN'		=> '',
    'QUEUE_IRON_PROJECT'	=> '',
    'QUEUE_IRON_QUEUE'		=> '',
    'QUEUE_REDIS_QUEUE'		=> '',

    // Stripe
    'STRIPE_SECRET'         => '',

    // Mandrill
    'MANDRILL_KEY'          => ''
];
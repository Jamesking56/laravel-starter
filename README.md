# Jamesking56's Laravel 4 Starter #

This project is an easy bootstrap for Laravel. I use it as a base for all of my projects.

## Installation ##

__DO NOT CLONE THIS REPO!__ Please download this repo as an archive first then you may use it. If you clone this repo, you may accidentally commit .env.php files (Which is a really bad practice!)

### What features does it have? ###

* .env.php files for sensitive configs
* helpers.php file and psr-0 configuration template (ready for your own namespace)
* Packages I normally use (you don't have to use these if you don't want to).

### Usage ###

You can utilise this in two ways:

1. Download as a ZIP and drop it into your project folder.
2. Use composer: `composer create-project jamesking56/laravel-starter yourfoldername --prefer-dist`

### Contribution guidelines ###

Feel free to submit a Pull Request. I may however modify your code to document it better or follow my own writing style.

### License ###

Uses the same as Laravel/Laravel (MIT)